# puppet-fail2ban

Puppet-fail2ban is a Puppet module implementing management of fail2ban.


## Usage

Install the module in the location of your choice.

My preference is to 

    include fail2ban

and set everything in hiera.

    fail2ban::config::filters:
        nginx-noscript:
            failregex: '^<HOST> -.*GET.*(\.php|\.asp|\.exe|\.pl|\.cgi|\.scgi)'
        nginx-proxy:
            failregex: '^<HOST> -.*GET http.*'
        nginx-shellshock:
            failregex: '^<HOST>,.+?,".*?\(.*?\).*?{.*$'

    fail2ban::config::jails:
        DEFAULT:
            bantime: 86400
        nginx-noscript:
            enabled: 'true'
            port: 'http,https'
            filter: 'nginx-noscript'
            logpath: '/var/log/nginx/access.log'
            maxretry: 2
        nginx-proxy:
            enabled: 'true'
            action: 'iptables-multiport[name=NoProxy, port="http,https"]'
            filter: 'nginx-proxy'
            logpath: '/var/log/nginx/access.log'
            maxretry: 1
        nginx-shellshock:
            bantime: 864000
            enabled: 'true'
            port: 'http,https'
            filter: 'nginx-shellshock'
            logpath: '/var/log/nginx/access.log'
            maxretry: 1


## Class fail2ban::package

Ensures that fail2ban is installed.

## Class fail2ban::service

Wraps a service resource for fail2ban.  It is intended only for restart following a 
configuration change, as I do not believe Puppet should be managing services.

## Class fail2ban::config

Provides configuration for fail2ban; this is where the action is.

### Parameters

Fail2ban uses its defaults for parameter values omitted.

* $log_level=undef
* $log_target=undef
* $socket=undef
* $pidfile=undef
* $actions={} # a hash of fail2ban::action resources.
* $filters={} # a hash of fail2ban::filter resources.
* $jails={} # a hash of fail2ban::jail resources.
* $conf_dir='/etc/fail2ban'
    
    
### Resource fail2ban::action

Currently unimplemented.

### Resource fail2ban::filter

Represents a fail2ban filter.

#### Parameters

* $failregex
* $ignoreregex=''
* $dir # intended to be private.
### Resource fail2ban:: jail

Represents a fail2ban jail.

#### Parameters

* $action=undef
* $backend=undef
* $bantime=undef
* $enabled=undef 
* $failregex=undef
* $filter=undef 
* $findtime=undef
* $ignorecommand=undef
* $ignoreip=undef
* $ignoreregex=undef
* $logpath=undef 
* $maxretry=undef
* $port=undef 
* $usedns=undef 
* $dir # intended to be private.

