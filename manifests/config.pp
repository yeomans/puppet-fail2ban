class fail2ban::config
    (
    $log_level=undef, 
    $log_target=undef,
    $socket=undef,
    $pidfile=undef,
    $actions={}, 
    $filters={},
    $jails={},
    $conf_dir='/etc/fail2ban'
    )
    {
    # these directories should have been created by fail2ban package.
    $action_d = "$conf_dir/action.d"
    $filter_d = "$conf_dir/filter.d"
    $jail_d = "$conf_dir/jail.d"
    
    file
        {
        ["$conf_dir", "$action_d", "$filter_d", "$jail_d"]:
        ensure => directory, 
        owner => "root", 
        group => "root", 
        mode => '0644',
        }

    file
        {
        "$conf_dir/fail2ban.local":
        ensure => file, 
        owner => "root", 
        group => "root", 
        mode => '0644',
        content => template("fail2ban/fail2ban.local.erb")
        }        
    
    create_resources(fail2ban::config::action, $actions, {dir=>"$action_d"})
    create_resources(fail2ban::config::filter, $filters, {dir=>"$filter_d"})
    create_resources(fail2ban::config::jail, $jails, {dir=>"$jail_d"})
    }
