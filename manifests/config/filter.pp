define fail2ban::config::filter
    (
    $failregex, 
    $ignoreregex='', 
    # $dir is intended to be private, and set in create_resource.
    $dir
    )
    {
    file
        {
        "$dir/$name.local":
        ensure => present, 
        owner => 'root', 
        group => 'root', 
        mode => '0644',
        content => template('fail2ban/filter.local.erb'),
        notify => Class['fail2ban::service']
        }
    }