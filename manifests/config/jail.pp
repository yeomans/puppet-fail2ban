define fail2ban::config::jail
    (
    $action=undef,
    $backend=undef,
    $bantime=undef,
    $enabled=undef, 
    $failregex=undef,
    $filter=undef, 
    $findtime=undef,
    $ignorecommand=undef,
    $ignoreip=undef,
    $ignoreregex=undef,
    $logpath=undef, 
    $maxretry=undef,
    $port=undef, 
    $usedns=undef,
    
    # $dir is intended to be private, and set in create_resource.
    $dir
    )
    {
    file
        {
        "$dir/$name.local":
        ensure => file, 
        owner => "root", 
        group => "root", 
        mode => '0644',
        content => template("fail2ban/jail.local.erb")
        }
    }
