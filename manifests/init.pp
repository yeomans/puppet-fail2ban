class fail2ban(Array $requires=[])
    {
    require $requires
    class{"fail2ban::package":} -> class{"fail2ban::config":} ~> class{"fail2ban::service":}
    }
    